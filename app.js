var fs = require("fs");
var marked = require('marked');
var renderer = new marked.Renderer();
var ejs = require('ejs');
var dir = require('node-dir');
var jsondir = require('jsondir');

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: false,
  pedantic: false,
  sanitize: true,
  smartLists: true,
  smartypants: false
});

marked.setOptions({
  highlight: function (code) {
  	var op = require('./js/highlight.js').highlightAuto(code).value;
    
    return op;
  }
});

function cl(msg){
	console.log(msg);
}

// renderer.heading = function (text, level) {
//   var escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');

//   return '<h' + level + '><a name="' +
//                 escapedText +
//                  '" class="anchor" href="#' +
//                  escapedText +
//                  '"><span class="header-link"></span></a>' +
//                   text + '</h' + level + '>';
// };

function readFile(filePath){
	return fs.readFileSync(filePath, 'ascii').toString();;
}

function writeFile(filePath, contents){
	return fs.writeFileSync(filePath, contents, 'utf-8');
}

function convertToHtml(){
	var indexTemplate = readFile('index.ejs');
	var inputMarkdown = readFile('readme.md');
	var outputBodyHtml = marked(inputMarkdown);
	
	var outputFullHtml = ejs.render(indexTemplate, {'htmlData': outputBodyHtml});

	writeFile('index.html', outputFullHtml);
}


convertToHtml();
//
// dir.readFiles('.', {
// 	// match: /.md$/,
// 	matchDir: ['docs'],
// 	exclude: [/^\./, 'node_modules']
// 	}, function(err, content, next) {
// 	    if (err) throw err;
// 	    //console.log('content:', content);
// 	    next();
// 	},
// 	function(err, files){
// 	    if (err) throw err;
// 	    console.log('finished reading files:', JSON.stringify(files));
// 	}
// );

// jsondir.dir2json('./docs', function(err, results) {
//     if (err) throw err;
//     console.log(JSON.stringify(results));
// });
